SELECT *
FROM ENGRAIS;

SELECT *
FROM PLANTE;

SELECT *
FROM MESURE;

SELECT *
FROM TEST;

/**
  Exercise 1
 */
SELECT LIBELLE
FROM ENGRAIS
WHERE IDENGRAIS IN
      (SELECT DISTINCT CODE_E
       FROM PLANTE
                RIGHT JOIN TEST T on PLANTE.IDPLANTE = T.CODE_P);

/**
  Exercise 2
 */
SELECT COUNT(DISTINCT FAMILLE) as nbFamille
FROM PLANTE;

/**
  Exercise 3
 */
SELECT DISTINCT famille
FROM Mesure
         RIGHT JOIN Plante ON Plante.IdPlante = Mesure.NumPlante
WHERE Mesure.NumPlante IS NULL;

/**
  Exercise 4
 */
SELECT famille
FROM Plante
         MINUS
         (SELECT famille
 FROM Plante
          INNER Join Mesure ON Plante.IdPlante = Mesure.NumPlante);

/**
  Exercise 5
 */
SELECT DISTINCT m1.NUMPLANTE
FROM MESURE m1
         JOIN MESURE m2 on m1.NUMPLANTE = m2.NUMPLANTE
WHERE m2.DATE_M > M1.DATE_M
  AND m2.TAILLE - m1.TAILLE > 5;

/**
  Exercise 6
 */
SELECT numPlante, date_m, taille
FROM Mesure m1
WHERE taille = (SELECT MAX(taille) FROM MESURE m2 WHERE m1.NUMPLANTE = m2.NUMPLANTE);

/**
  Exercise 7
 */
SELECT libelle, count(code_e)
FROM test
         RIGHT JOIN engrais ON code_e = idengrais
GROUP BY code_e, libelle;

/**
  Exercise 8
 */
SELECT IdPlante, IdEngrais, NVL(sum(quantite), 0)
FROM Plante,
     Engrais
         LEFT JOIN test ON test.code_e = Engrais.idengrais
GROUP BY IdPlante, IdEngrais
ORDER BY 1, 2;

/**
  Exercise 9
 */
SELECT CODE_P, ROUND(AVG(QUANTITE)), COUNT(IDENGRAIS)
FROM TEST
         inner join ENGRAIS E on E.IDENGRAIS = TEST.CODE_E
WHERE LIBELLE = 'pyrene'
GROUP BY CODE_P
HAVING COUNT(IDENGRAIS) >= 3;

/**
  Exercise 10
 */
SELECT M1.numPlante, M2.taille - M1.taille AS Evolution
FROM (SELECT M1.numPlante, M1.Date_M AS date_1, min(M2.Date_M) AS date_2
      FROM Mesure M1,
           Mesure M2
      WHERE M1.numPlante = M2.numPlante
        AND M1.Date_M < M2.Date_M
      Group by M1.numPlante, M1.Date_M) tab
         INNER JOIN Mesure M1 ON tab.numPlante = M1.numPlante
         INNER JOIN Mesure M2 on M1.numPlante = M2.numPlante
WHERE tab.date_1 = M1.date_M
  AND M2.date_M = tab.date_2
ORDER BY 1, 2;

/**
  Exercise 11
 */
SELECT NUMPLANTE, MAX(TAILLE) - MIN(TAILLE) as DIFFERENCE
FROM MESURE
GROUP BY NUMPLANTE
ORDER BY 2 DESC;

/**
  Exercise 12
 */
SELECT NUMPLANTE, "consecutiveEvolution" as "highestConsecutiveEvolution"
FROM (SELECT M1.numPlante, M2.taille - M1.taille AS "consecutiveEvolution"
      FROM (SELECT M1.numPlante, M1.Date_M AS date_1, min(M2.Date_M) AS date_2
            FROM Mesure M1,
                 Mesure M2
            WHERE M1.numPlante = M2.numPlante
              AND M1.Date_M < M2.Date_M
            Group by M1.numPlante, M1.Date_M) tab
               INNER JOIN Mesure M1 ON tab.numPlante = M1.numPlante
               INNER JOIN Mesure M2 on M1.numPlante = M2.numPlante
      WHERE tab.date_1 = M1.date_M
        AND M2.date_M = tab.date_2
      ORDER BY 2 DESC)
WHERE ROWNUM = 1;

/**
  Exercise 13
 */
SELECT NUMPLANTE, "evolution" as "highestOverallEvolution"
FROM (SELECT NUMPLANTE, MAX(TAILLE) - MIN(TAILLE) as "evolution"
      FROM MESURE
      GROUP BY NUMPLANTE
      ORDER BY 2 DESC)
WHERE ROWNUM = 1;

/**
  Exercise 14
 */
SELECT NUMPLANTE, MAX(DATE_M) - MIN(DATE_M) as "delayBetweenFirstAndLast"
FROM MESURE
GROUP BY NUMPLANTE;

/**
  Exercise 15
 */
/**
  CREATE INTEGRITY ON TEST IS QTE=<50, créer une contrainte de quantitié sur tout les tests, à l'éxecution de la commande,
  la contrainte doit être respecté sinon la commande ne fonctionnera pas, après avoir éxécuté la commande, les futures
  commandes doivent respecté la contrainte c'est à dire que les tests ne doivent pas dépassé une quantité de 50.
 */

/**
  Exercise 16
 */
INSERT INTO PLANTE
VALUES ('or1', 'Orchidé');
INSERT INTO PLANTE
VALUES ('tu1', 'Tulipe');

/**
  Exercise 17
 */
DELETE
FROM PLANTE
WHERE IDPLANTE IN (SELECT M1.NUMPLANTE
                   FROM (SELECT M1.numPlante, M1.Date_M AS date_1, min(M2.Date_M) AS date_2
                         FROM Mesure M1,
                              Mesure M2
                         WHERE M1.numPlante = M2.numPlante
                           AND M1.Date_M < M2.Date_M
                         Group by M1.numPlante, M1.Date_M) tab
                            INNER JOIN Mesure M1 ON tab.numPlante = M1.numPlante
                            INNER JOIN Mesure M2 on M1.numPlante = M2.numPlante
                   WHERE tab.date_1 = M1.date_M
                     AND M2.date_M = tab.date_2
                     AND M2.TAILLE - M1.TAILLE < 5);
